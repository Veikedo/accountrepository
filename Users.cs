﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccountRepository
{
    class Users : IAccountService
    {
        private readonly Repository repository;

        public Users(Repository repository)
        {
            this.repository = repository;
        }

        public void AddAccount(Account account)
        {
            CheckData(account);
            repository.Add(account);
        }

        private void CheckData(Account account)
        {
            var date = DateTime.Now.AddYears(-18);
            if (account.BirthDate > date)
            {
                throw new Exception("Регистрация возможно только после 18 лет.");
            }
            if (string.IsNullOrEmpty(account.FirstName))
            {
                throw new Exception("Не указано имя.");
            }
            if (string.IsNullOrEmpty(account.LastName))
            {
                throw new Exception("Не указана фамилия.");
            }

            if (repository.GetAll().Any(w =>
                w.FirstName == account.FirstName && w.LastName == account.LastName &&
                account.BirthDate == account.BirthDate))
            {
                throw new Exception("Такая учетная запись уже существует.");
            }
        }
    }
}
